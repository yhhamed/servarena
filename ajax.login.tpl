{if $loginwidget}

    <div id="login-widget" {if !$loginpage}style="display: none"{/if}>
        <form id="login-widget-form" name="" action="{$system_url}?redirect=?cmd=clientarea" method="post">

            <div class="row-fluid">
                <div class="span3" id="left_login">
                    {if !$loginpage}
                        <div id="login-widget-close">
                            <span class="entypo">&#10060;</span>
                        </div>
                    {/if}
                    <div class="vt-venter">
                        <div class="left_container">
                            <hgroup>
                                <h4>{$lang.welcometo}</h4>
                                <h1>{$business_name}</h1>
                            </hgroup>
                            <div class="login-divider"></div>
                            <div class="create_account">
                                <a href="{$system_url}signup/" title="{$lang.createaccount}">
                                    <span class="entypo">&#59136;</span>
                                </a>
                                <div>
                                    <span>{$lang.signin}</span>
                                    <a href="{$system_url}signup/">{$lang.createaccount}</a>
                                </div>
                            </div>
                        </div><!-- /left_container-->
                    </div>
                    <div class="login_footer">
                        <div class="login-divider"></div>
                        <h6></h6>
                    </div><!-- /login_footer-->
                </div><!-- /span3-->
                <div class="span9 form-horizontal" id="right_login">
                    <div class="vt-venter">
                        <div class="right_container">
                            <h1>{$lang.login}</h1>
                            <div class="top_divider"></div>
                            <h4>{$lang.pleaseloginyouraccount}:</h4>
                            <div class="login-input row-fluid">
                                <span class="span2">{$lang.login}</span>
                                <div class="input-prepend span10">
                                    <span class="entypo add-on">&#128100;</span>
                                    <input id="login-widget-user" name="username" type="text" required>
                                </div>
                            </div>
                            {if $enableFeatures.logincaptcha =='on'}
                                <div class="login-input row-fluid">
                                    <span class="span2">{$lang.captcha}</span>

                                    <div class="input-prepend input-append captcha-input span10">

                                        <div class="captcha-bg add-on" id="logincaptcha-bg" style="background-image: url(?cmd=root&action=captcha#w{$timestamp})"></div>
                                        <input type="text" name="captcha" />
                                        <i class="add-on fa fa-refresh" onclick="$('#logincaptcha-bg').css('background-image', 'url(?cmd=root&action=captcha&t=' + (new Date()).getTime() + ')');"></i>
                                        <a href="#" onclick="" >{$lang.refresh}</a>
                                    </div>
                                </div>
                            {/if}
                            <div class="login-input row-fluid">
                                <span class="span2">{$lang.password}</span>
                                <div class="input-prepend span10">
                                    <span class="entypo add-on">&#128274;</span>
                                    <input type="password" autocomplete="off" name="password" id="login-widget-password" required />
                                </div>
                            </div>
                            <div id="login-widget-option pull-right"> 
                                <a href="{$system_url}root&action=passreminder" class="list_item pull-right">{$lang.forgotpassword}</a>
                            </div>
                            <button type="submit" class="btn progress-button" id="login-widget-submit" 
                                    data-horizontal="" data-style="fill">Login</button>      
                        </div><!-- /right_container-->
                    </div>
                    <div class="login_footer">
                        <div class="login-divider"></div>
                        <h6>All rights reserved &copy; Companyname 2015</h6>
                    </div><!-- /login_footer-->
                </div><!-- /span9-->
            </div><!-- /row-fluid-->

            <input type="hidden" name="action" value="login"/>
            {securitytoken}
        </form>
    </div>
{else}

    {include file="../common/tpl/ajax.login.tpl"}
{/if}

<div class="row m-form__group">
    <div class="col-md-6 mb-3">
        {foreach from=$fields item=field name=floop key=k}
        {if $smarty.foreach.floop.iteration > ($smarty.foreach.floop.total/2)}{break}{/if}
        <div {if $field.type=='Company' && $fields.type}class="iscomp form-group m-form__group"  style="{if !$submit.type || $submit.type=='Private'}display:none{/if}"
        {elseif $field.type=='Private' && $fields.type}class="ispr form-group m-form__group"  style="{if $submit.type=='Company'}display:none{/if}"{else} class="form-group m-form__group"{/if}>
            <label for="field_{$k}" >
                {if $k=='type'}
                    {$lang.clacctype}
                {elseif $field.options & 1}
                    {if $lang[$k]}{$lang[$k]}{else}{$field.name}{/if}
                {else}
                    {$field.name}
                {/if}
                {if $field.options & 2}*{/if}
                {if $field.description}<span  data-container="body" data-toggle="m-tooltip" data-placement="top" title="{$field.description|htmlspecialchars}">?</span>{/if}
            </label>
            {if $k=='type'}
                <select  id="field_{$k}" name="type" class="form-control m-input" onchange="{literal}if ($(this).val()=='Private') {$('.iscomp').hide();$('.ispr').show();}else {$('.ispr').hide();$('.iscomp').show();}{/literal}">
                    <option value="Private" {if $submit.type=='Private'}selected="selected"{/if}>{$lang.Private}</option>
                    <option value="Company" {if $submit.type=='Company'}selected="selected"{/if}>{$lang.Company}</option>
                </select>
            {elseif $k=='country'}
                <select name="country" class="form-control m-input" id="field_{$k}" >
                    {foreach from=$countries key=k item=v}
                        <option value="{$k}" {if $k==$submit.country  || (!$submit.country && $k==$defaultCountry)} selected="Selected"{/if}>{$v}</option>
                    {/foreach}
                </select>
            {else}
                {if $field.field_type=='Input'}
                    {if  $k=='captcha'}
                        <div style="white-space: nowrap;">
                            <img class="capcha" style="width: 120px; height: 60px;" src="?cmd=root&action=captcha#{$stamp}" /> 
                            <span style="display: inline-block; width: 65%; white-space: normal;">
                                {$lang.typethecharacters}<br />
                                <a href="#" onclick="return singup_image_reload();" >{$lang.refresh}</a>
                            </span>
                        </div>
                    {/if}
                    <input type="text" placeholder="{$field.placeholder}" value="{$submit[$k]}" name="{$k}" class="form-control m-input" id="field_{$k}" />
                {elseif $field.field_type=='Password'}
                    <input type="password" autocomplete="off" value=""  name="{$k}" class="form-control m-input" id="field_{$k}" />
                {elseif $field.field_type=='Select'}
                    <select name="{$k}" class="form-control m-input" id="field_{$k}" >
                        {foreach from=$field.default_value item=fa}
                            <option {if $submit[$k]==$fa}selected="selected"{/if}>{$fa}</option>
                        {/foreach}
                    </select>
                {elseif $field.field_type=='Check'}
                    {foreach from=$field.default_value item=fa}
                        <input class="form-control m-input" type="checkbox" name="{$k}[{$fa|escape}]" {if $submit[$k][$fa]}checked="checked"{/if} value="1" />{$fa}<br />
                    {/foreach}
                {/if}
            {/if}
        </div>
        {/foreach}
    </div>
    <div class="col-md-6 mb-3">
            {foreach from=$fields item=field name=floop key=k}
            {if $smarty.foreach.floop.iteration <= ($smarty.foreach.floop.total/2)}{continue}{/if}
            <div {if $field.type=='Company' && $fields.type}class="iscomp form-group m-form__group" style="{if !$submit.type || $submit.type=='Private'}display:none{/if}"
            {elseif $field.type=='Private' && $fields.type}class="ispr form-group m-form__group" style="{if $submit.type=='Company'}display:none{/if}" {else} class="form-group m-form__group"{/if}>
                <label for="field_{$k}" >
                    {if $k=='type'}
                        {$lang.clacctype}
                    {elseif $field.options & 1}
                        {if $lang[$k]}{$lang[$k]}{else}{$field.name}{/if}
                    {else}
                        {$field.name}
                    {/if}
                    {if $field.options & 2}*{/if}
                    {if $field.description}<span  data-container="body" data-toggle="m-tooltip" data-placement="top" title="{$field.description|htmlspecialchars}">?</span>{/if}
                </label>
                {if $k=='type'}
                    <select  id="field_{$k}" name="type" class="form-control m-input" onchange="{literal}if ($(this).val()=='Private') {$('.iscomp').hide();$('.ispr').show();}else {$('.ispr').hide();$('.iscomp').show();}{/literal}">
                        <option value="Private" {if $submit.type=='Private'}selected="selected"{/if}>{$lang.Private}</option>
                        <option value="Company" {if $submit.type=='Company'}selected="selected"{/if}>{$lang.Company}</option>
                    </select>
                {elseif $k=='country'}
                    <select name="country" class="form-control m-input" id="field_{$k}" >
                        {foreach from=$countries key=k item=v}
                            <option value="{$k}" {if $k==$submit.country  || (!$submit.country && $k==$defaultCountry)} selected="Selected"{/if}>{$v}</option>
                        {/foreach}
                    </select>
                {else}
                    {if $field.field_type=='Input'}
                        {if  $k=='captcha'}
                            <div style="white-space: nowrap;">
                                <img class="capcha" style="width: 120px; height: 60px;" src="?cmd=root&action=captcha#{$stamp}" /> 
                                <span style="display: inline-block; width: 65%; white-space: normal;">
                                    {$lang.typethecharacters}<br />
                                    <a href="#" onclick="return singup_image_reload();" >{$lang.refresh}</a>
                                </span>
                            </div>
                        {/if}
                        <input type="text" placeholder="{$field.placeholder}" value="{$submit[$k]}" name="{$k}" class="form-control m-input" id="field_{$k}" />
                    {elseif $field.field_type=='Password'}
                        <input type="password" autocomplete="off" value=""  name="{$k}" class="form-control m-input" id="field_{$k}" />
                    {elseif $field.field_type=='Select'}
                        <select name="{$k}" class="form-control m-input" id="field_{$k}" >
                            {foreach from=$field.default_value item=fa}
                                <option {if $submit[$k]==$fa}selected="selected"{/if}>{$fa}</option>
                            {/foreach}
                        </select>
                    {elseif $field.field_type=='Check'}
                        {foreach from=$field.default_value item=fa}
                            <input class="form-control m-input" type="checkbox" name="{$k}[{$fa|escape}]" {if $submit[$k][$fa]}checked="checked"{/if} value="1" />{$fa}<br />
                        {/foreach}
                    {/if}
                {/if}
            </div>
            {/foreach}
        </div>
</div>
{literal}
    <script>
        $(document).ready(function(){
            {/literal}{if $cmd=='cart' && $step!=2}{literal}$("#field_country, #state_input, #state_select").change(function(){var e=this;$.post("?cmd=cart&action=regionTax",{country:$("#field_country").val(),state:$("#state_select:visible").length>0?$("#state_select").val():$("#state_input").val()},function(t){if(t==1){$(e).parents("form").append('<input type="hidden" name="autosubmited" value="true" />').submit()}})}){/literal}{/if}{literal}
            //$(".chzn-select").chosen();
        });
        function singup_image_reload(){
            var d = new Date();    
            $('.capcha:first').attr('src', '?cmd=root&action=captcha#' + d.getTime());
            return false;
        }
    </script>

{/literal}
{if $getRenewPeriods}
    {if $renew_prices}
        <form action="" method="post">
            <input type="hidden" name="submit" value="1" />
            <input type="hidden" name="renew" value="1" />
            {$lang.chooseperiod}:
            <select class="form-control m-input" name="period">
                {if $renew_prices}
                    {foreach from=$renew_prices item=rprice}
                        <option value="{$rprice.period}">{$rprice.period} {$lang.years} @ {$rprice.renew|price:$currency}</option>
                    {/foreach}
                {/if}
            </select>
            <input  type="submit" name="do_renew" value="{$lang.orderrenewal}"  class="btn btn-success" />
            <input type="button" value="{$lang.cancel}" onclick="$(this).parent().parent().hide().removeClass('shown');
                    return false;" class="btn " />
            {securitytoken}</form>
        {else}
        <div class="alert alert-danger" role="alert">{$lang.renewnotavailable}</div>
    {/if}
{else}
    {if $domains}
        {foreach from=$domains item=domain name=foo}
            <tr>
                <td>
                    <a href="{$system_url}clientarea/domains/{$domain.id}/{$domain.name}/" class="roll-link">
                        <span data-title="{$domain.name}">{$domain.name}</span>
                    </a>
                    {if $domain.status == 'Active' || $domain.status == 'Expired'}
                        {if $domain.daytoexpire < 60 && $domain.daytoexpire >= 0}
                            <span class="m-badge  m-badge--warning">
                                {$domain.daytoexpire} 
                                {if $domain.daytoexpire==1}{$lang.day}
                                {else}{$lang.days}
                                {/if} {$lang.toexpire}!
                            </span>
                        {/if}
                    {/if}
                </td>
                <td>{if !$domain.date_created || $domain.date_created == '0000-00-00'}-{else}{$domain.date_created|dateformat:$date_format}{/if}</td>
                <td>
                    {if !$domain.expires || $domain.expires == '0000-00-00' || ($domain.status!='Active' && $domain.status!='Expired') }-
                    {else}{$domain.expires|dateformat:$date_format}
                    {/if}
                </td>
                <td>{if $domain.status != 'Expired'}1{else}2{/if}</td>
                <td> 
                    {if $domain.status == 'Active' || $domain.status == 'Expired'}
                        {if $domain.autorenew=='0'}
                            <span>{$lang.Off}</span>
                        {else}<span>{$lang.On}</span>
                        {/if}
                    {else}-
                    {/if}
                </td>
                <td>
                    {if $domain.status=='Active' || $domain.status == 'Expired'}
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--align-push"
                        m-dropdown-toggle="click" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link">
                            <i class="la la-ellipsis-v"></i>
                        </a>
                        <div class="m-dropdown__wrapper">
                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                            <li class="m-nav__item">
                                                <a href="{$system_url}clientarea/domains/{$domain.id}/{$domain.name}/" class="m-nav__link">
                                                    <i class="m-nav__link-icon  flaticon-settings"></i>
                                                    <span class="m-nav__link-text">{$lang.managedomain}</span>
                                                </a>
                                            </li>
                                            <li class="m-nav__item">
                                                <a href="{$system_url}clientarea/domains/&id=renew&ids[]={$domain.id}" class="m-nav__link">
                                                    <i class="m-nav__link-icon flaticon-share"></i>
                                                    <span class="m-nav__link-text">{$lang.renew_widget}</span>
                                                </a>
                                            </li>
                                            {if $domwidgets}
                                                {foreach from=$domwidgets item=widg}
                                            <li class="m-nav__item">
                                                <a href="{$system_url}clientarea/domains/{$domain.id}/{$domain.name}/&widget={$widg.widget}#{$widg.widget}" class="m-nav__link">
                                                    {assign var=widg_name value="`$widg.name`_widget"}
                                                        {if $lang[$widg_name]}{$lang[$widg_name]}
                                                        {elseif $lang[$widg.widget]}{$lang[$widg.widget]}
                                                        {else}{$widg.name}
                                                    {/if}
                                                </a>
                                                </li>
                                                {/foreach}
                                            {/if}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {else}
                    <a href="{$system_url}clientarea/domains/{$domain.id}/{$domain.name}/" class="btn btn-small btn-primary">
                        <i class="icon-cog"></i>
                    </a>
                    {/if}
                </td>
            </tr>
        {foreachelse}
            <tr><td>{$lang.nothing}</td></tr>
        {/foreach}
    {/if}
{/if}
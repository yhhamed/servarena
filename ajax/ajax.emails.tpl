{if $emails}

{foreach from=$emails item=email name=foo}
										<tr>
											<td>
													<span><a href="{$system_url}clientarea/emails/{$email.id}/" >{$email.subject}</a></span>
											</td>
											<td><span>{$email.date|dateformat:$date_format}</span></td>
										</tr>
										{/foreach}

{else}

    <tr><td>{$lang.nothing}</td></tr>

{/if}
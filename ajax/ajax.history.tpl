{if $logs}
    {foreach from=$logs item=log name=foo}
        <tr>
            <td>{$log.date|dateformat:$date_format}</td>
            <td>{$log.description}</td>
            <td><span class="text-center"><i class="m-badge m-badge--{if $log.result}success{else}danger{/if} m-badge--wide"></i></span></td>
            <td>
                {if $log.type == 'account'}
                <span class="text-center" data-title="ID {$log.id}">
                    <a href="?cmd=clientarea&action=services&service={$log.id}" target="_blank"  class="roll-link">
                        ID {$log.id}
                    </a>
                    </span>
                {elseif $log.type=='domain'}
                    <span class="text-center" data-title="ID {$log.id}">
                        <a href="?cmd=clientarea&action=domains&id={$log.id}" target="_blank"  class="roll-link">
                            ID {$log.id}
                        </a>
                    </span>
                {else}
                <span>
                    <a href="#" target="_blank" onclick="return false;" class="roll-link">
                        
                    </a>
                    </span>
                {/if}
            </td>
        </tr>
        
    {/foreach}
    {else}
        <tr><td class="col-12">{$lang.nothing}</td></tr>
{/if}
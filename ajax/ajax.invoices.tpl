
{if $invoices}
    {foreach from=$invoices item=invoice name=foo}
        <tr>
            <td>

                {if $invoice.status != 'Unpaid'}1{else}2{/if}
            </td>
            <td><span >
                <a href="{$system_url}clientarea/invoice/{$invoice.id}/" class="roll-link">
                    <span data-title="#{$invoice|@invoice}">#{$invoice|@invoice}
                              </span>
                          </a>
                          </span>
                    </td>
                    <td><span>{$invoice.total|price:$invoice.currency_id}</span></td>
                    <td><span>{$invoice.date|dateformat:$date_format}</span></td>
                    <td>
                        {$invoice.duedate|dateformat:$date_format}
                    </td>
                    <td>
                    <span >
                        <a href="{$system_url}clientarea/invoice/{$invoice.id}/">
                            <i class=" flaticon-tool-1"></i>
                        </a>
                        </span>
                    </td>
                </tr>
                {foreachelse}
                    <tr><td>{$lang.nothing}</td></tr>
                    {/foreach}
{/if}
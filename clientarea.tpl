
<!-- Done -->
{if $action == 'details'}
    {include file='clientarea/editdetails.tpl'}
<!-- Done -->    
{elseif $action=='addfunds'}
   {include file='clientarea/addfunds.tpl'}
<!-- Done -->  
{elseif $action=='profilepassword'}
   {include file='contacts/changepassword.tpl'} 
 <!-- Done -->  
{elseif $action=='password'}
   {include file='clientarea/changepassword.tpl'}
 <!-- Done -->    
{elseif $action=='ipaccess'}
	{include file='clientarea/ipaccess.tpl'}
 <!-- Done --> 
{elseif $action=='ccard'}
	{include file='clientarea/creditcard.tpl'}
 <!-- Done --> 
{elseif $action=='ach'}
	{include file='clientarea/ach.tpl'}
 <!-- Need 3D Secury -->     
{elseif $action=='ccprocessing'}
	{include file='clientarea/ccprocessing.tpl'}
 <!-- Done -->     
{elseif $action=='emails'}
	{include file='clientarea/emails.tpl'}
 <!-- Done -->    
{elseif $action=='history'}
	{include file='clientarea/history.tpl'} 
 <!-- Done -->     
{elseif $action=='invoices'}
	{include file='clientarea/invoices.tpl'}
    
{elseif $action=='domains'}
	{include file='services/domains.tpl'}
    
{elseif $action=='services' || $action=='accounts' || $action=='reseller' || $action=='vps' || $action=='servers'}
	{include file='services/services.tpl'}
    
{elseif $action=='overview'}
    {include file='clientarea/overview.tpl'}

{elseif $action=='delete'}
    {include file='clientarea/delete.tpl'}

{elseif $action=='settings'}
    {include file='clientarea/settings.tpl'}

{elseif $action=='cancel'}
	{include file='services/cancelationrequest.tpl'}
    
{else}
	{include file='clientarea/dashboard.tpl'}
{/if}



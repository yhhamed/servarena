
    <div class="m-content">
            {if $ach.account}
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                        {$lang.ach_information}
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table table-striped m-table">
                                    <tbody>
                                        <tr>
                                            <th>{$lang.type}</th>
                                            <td>{$lang[$ach.type]}</td>
                                        </tr>
                                        <tr>
                                            <th>{$lang.ach_account_number}</th>
                                            <td>{$ach.account}</td>
                                        </tr>
                                        <tr>
                                            <th>{$lang.ach_routing_number}</th>
                                            <td>{$ach.routing}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" method="post">
                    {if $allow_storage}
                        <a href="#newccdetails" data-toggle="modal" class="btn btn-primary" >{$lang.changeach}</a>
                    {/if}
                    {if $allowremove}
                        <button   class="btn btn-danger" type="submit" name="removecard" onclick="return confirm('{$lang.removeach_confirm}')" >{$lang.removecc}</button>
                    {/if}
                    {securitytoken}
                </form>
            {else}
                <h4>{$lang.ach_information}</h4>
                <div class="m-alert m-alert--icon alert alert-primary" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        {$lang.noachyet}
                    </div>
                    <div class="m-alert__actions" style="width: 160px;">
                        <a href="#newccdetails" class="btn btn-primary" data-toggle="modal"><i class="icon-add"></i> {$lang.newach}</a>
                    </div>
                </div>
            {/if}
            <div class="modal fade" id="newccdetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{$lang.changeachdesc}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="post">
                            <input type="hidden" name="addach" value="1" />
                            <div class="col-12">
                                <div class="mb-3">
                                    <label for="type">{$lang.type}</label>
                                    <select name="type" class="form-control m-input">
                                        <option value="checkings">{$lang.checking}</option>
                                        <option value="savings">{$lang.savings}</option>
                                        <option value="business_checking">{$lang.business_checking}</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="number">{$lang.ach_account_number}</label>
                                    <input type="text" name="account" class="form-control m-input" />
                                </div>
                                <div class="mb-3">
                                    <label for="number">{$lang.ach_routing_number}</label>
                                    <input type="text" name="routing" class="form-control m-input" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{$lang.close}</button>
                            <button type="submit" name="addcard" class="btn btn-primary">{$lang.savechanges}</button>
                        </div>
                        {securitytoken}
                    </form>
                    </div>
                </div>
            </div>
    </div>

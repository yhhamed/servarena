
    <div class="m-content">
        <div class="alert alert-info" role="alert">
            {$lang.MinDeposit}: <strong>{$mindeposit|price:$currency}</strong>
            {$lang.MaxDeposit}:<strong> {$maxdeposit|price:$currency}</strong>
        </div>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {$lang.addfunds}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <span>{$lang.addfunds_d}</span>
                <form class="form-style" method="post" action="">
                        <input type="hidden" name="make" value="addfunds" />
                    <label for="number"></label>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="expirym">{$lang.trans_amount}<</label>
                            <input class="form-control m-input" type="text" name="funds" value="{$mindeposit|price:$currency:false}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="expirym">{$lang.trans_gtw}</label>
                            <select name="gateway" class="form-control m-input">
                                    {foreach from=$gateways key=gatewayid item=paymethod}
                                        <option value="{$gatewayid}">{$paymethod}</option>
                                    {/foreach}
                                </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"> {$lang.addfunds}</button>
                    {securitytoken}
                </form>
            </div>
        </div>


    </div>

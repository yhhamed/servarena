
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    {if $clientdata.contact_id}
                                        {$lang.changemainpass}
                                    {else}
                                        {$lang.changepass}
                                    {/if}
                                </h3>
                            </div>
                        </div>
                    </div>
        
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" action='' method='post'>
                    <input type="hidden" name="make" value="changepassword" />
                        <div class="m-portlet__body">
                                <div class="m-form__section">
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                            {$lang.oldpass}
                                        </label>
                                        <div class="col-lg-6">
                                            <input name="oldpassword" type="password" autocomplete="off"  class="form-control m-input"  />
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                            {$lang.newpassword}
                                        </label>
                                        <div class="col-lg-6">
                                            <input name="password" type="password" autocomplete="off"  class="form-control m-input"  />
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-2 col-form-label">
                                            {$lang.confirmpassword}
                                        </label>
                                        <div class="col-lg-6">
                                            <input name="password2" type="password" autocomplete="off"  class="form-control m-input"  />
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary ">{$lang.savechanges}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {securitytoken}
                    </form>
        
                    <!--end::Form-->
                </div>

                <!--end::Portlet-->            
            </div>
        </div>
    </div>

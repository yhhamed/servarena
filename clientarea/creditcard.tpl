
    <div class="m-content">
            
            {if $ccard.cardnum}
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                        {$lang.ccard}
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table table-striped m-table">
                                    <tbody>
                                        <tr>
                                            <th>{$lang.cctype}</th>
                                            <td>{$ccard.cardtype}</td>
                                        </tr>
                                        <tr>
                                            <th>{$lang.ccnum}</th>
                                            <td>{$ccard.cardnum}</td>
                                        </tr>
                                        <tr>
                                            <th>{$lang.ccexpiry}</th>
                                            <td>{$ccard.expdate}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" method="post">
                    {if $allow_storage}
                        <a href="#newccdetails" data-toggle="modal" class="btn btn-primary" >{$lang.changecc}</a>
                    {/if}
                    {if $allowremove}
                        <button class="btn btn-danger" type="submit" name="removecard" onclick="return confirm('{$lang.removecc_confirm}')" >{$lang.removecc}</button>
                    {/if}
                    {securitytoken}
                </form>
            {else}
                <h4>{$lang.ccard}</h4>
                <div class="m-alert m-alert--icon alert alert-primary" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        {$lang.noccyet}
                    </div>
                    <div class="m-alert__actions" style="width: 160px;">
                        <a href="#newccdetails" class="btn btn-primary" data-toggle="modal"><i class="icon-add"></i> {$lang.newcc}</a>
                    </div>
                </div>
            {/if}
            <div class="modal fade" id="newccdetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{$lang.changeccdesc}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="" method="post">
                            <div class="col-12">
                                <div class="mb-3">
                                    <label for="type">{$lang.cctype}</label>
                                    <select class="form-control m-input" name="cardtype">
                                        {foreach from=$supportedcc item=cc}
                                            <option>{$cc}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="number">{$lang.ccnum}</label>
                                    <input type="text" name="cardnum" class="form-control m-input" />
                                </div>
                                <label for="number">{$lang.ccexpiry}</label>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="expirym">MM</label>
                                        <input type="text" name="expirymonth" maxlength="2"  class="form-control m-input" />
                                    </div>
                                    <div class="col-md-6 mb-3">
                                            <label for="expirym">YY</label>
                                            <input type="text" name="expiryyear" maxlength="2"  class="form-control m-input"  />
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{$lang.close}</button>
                            <button type="submit" name="addcard" class="btn btn-primary">{$lang.savechanges}</button>
                        </div>
                        {securitytoken}
                    </form>
                    </div>
                </div>
            </div>
    </div>

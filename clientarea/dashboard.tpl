{*

Clientarea dashboard - summary of owned services, due invoices, opened tickets

*}
<div class="row-fluid" id="top_row">
    <h1>{$lang.dashboard}</h1>
</div>
<div class="spacing">
    
    <div class="row-fluid" id="rect">
        <div class="span3">
            <a href="{$system_url}tickets/">
                <span class="entypo">&#9993;</span>
                <div>
                    {$lang.tickets}
                    <span>{clientstats type=ticketTotal tpl="%d" default=0}</span>
                </div>
                
            </a>
        </div><!-- /span3 --> 
        <div class="span3">
            <a href="{$system_url}clientarea/invoices/">
                <span class="entypo">&#128197;</span>
                <div>
                    {$lang.invoices}
                    <span>{clientstats type=invoices tpl="%d" default=0}</span>
                </div>
            </a>
        </div><!-- /span3 --> 
        <div class="span3">
            <a href="{$system_url}clientarea/">
                <span class="entypo">&#59392;</span>
                <div>
                    {$lang.backtoser}
                    <span>{$client_services_total}</span>
                </div>
            </a>
        </div><!-- /span3 --> 
        <div class="span3">
            <a href="{$system_url}cart/">
                <span class="entypo">&#8862;</span>
                <div>{$lang.addnew}

                </div>
            </a>
        </div><!-- /span3 --> 
    </div>

    <div class="row-fluid" style="margin-top:20px;">
        <div class="span12">
            {clientwidget module="dashboard" section="blocks" wrapper="widget.tpl"}

        </div>
    </div>
    <div class="row-fluid" id="news">
        <h3>{$lang.my_services}</h3>
        <div class="solved">
            <span class="icon icon-EmptyBox"></span>
            <h6>{$lang.solved} <br> {$lang.ticketshort}</h6>
            <span class="text">{clientstats type=ticketClosed tpl="%d" default=0}</span>
        </div>
        <div class="opened">
            <span class="icon icon-EmptyBox"></span>
            <h6>{$lang.Open} <br> {$lang.ticketshort}</h6>
            <span class="text">{clientstats type=ticketOpen tpl="%d" default=0}</span>
        </div>
        <div class="solved" style="margin-right:30px;">
            <span class="icon icon-Mastercard"></span>
            <h6>{$lang.dueinvoices} <br> {if $acc_balance}{$acc_balance|price:$currency}{else}{0|price:$currency}{/if}</h6>
        </div>
        <div class="opened">
            <span class="icon icon-Dollar"></span>
            <h6>{$lang.balance} <br> {if $acc_credit}{$acc_credit|price:$currency}{else}{0|price:$currency}{/if}</h6>
        </div>
    </div>


    <div class="row-fluid">
        <div id="board-services">
            <div class="tabbable tabs-left span2 hidden-phone"> 
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="{$system_url}clientarea/#all"><span>{$lang.all}</span></a>
                    </li>
                    {if $offer_domains}
                        {clientservices}
                        <li>
                            <a href="{$system_url}clientarea/#domains"><span>{$lang.domains}</span></a>
                        </li>
                    {/if}
                    {counter name=srlistc print=false start=0 assign=srlistc}
                    {foreach from=$offer item=offe}
                        {clientservices}
                        {if $offe.total>0}
                            {counter name=srlistc}
                            
                            <li {if $srlistc > 3}style="display: none"{/if}>
                                <a href="{$system_url}clientarea/#{$offe.slug}"><span>{$offe.name}</span></a>
                            </li>
                            {assign value=true var=hasservice}
                        {/if}
                    {/foreach}
                </ul>
            </div> 
            <div class="slides-wrapper span10">
                <div class="wrapper">
                    <table class="table">
                        <tbody>
                            {if $offer_domains>0 || $hasservice}
                                {clientservices}
                                {counter name=srlist print=false start=0 assign=srlist}
                                {foreach from=$client_domains item=service}
                                    {counter name=srlist}
                                    <tr {if $srlist>3}style="display:none;"{/if} class="_all _domains">
                                        <td>
                                            <a class="bold" href="{$system_url}clientarea/domains/{$service.id}/{$service.name}/">
                                                <strong>{$service.name}</strong>
                                            </a>
                                        </td>
                                        <td><span class="label label-{$service.status}">{$lang[$service.status]}</span></td>
                                        <td><span class="visible-desktop">{$service.recurring_amount|price:$currency}</span></td>
                                        <td><span class="visible-desktop">{$service.period} {$lang.years}</span></td>
                                        <td ><span class="hidden-phone-portrait">
                                            {if $service.expires && $service.expires!='0000-00-00'}
                                                <small>{$lang.ccardexp}</small><br />
                                                <span>{$service.expires|dateformat:$date_format}</span>
                                            {else} - 
                                            {/if}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{$system_url}clientarea/domains/{$service.id}/{$service.name}/"><i class="icon-cog color-grey"></i></a>
                                        </td>
                                    </tr>
                                {/foreach}
                                {foreach from=$client_services item=service}
                                    {counter name=srlist}
                                    <tr {if $srlist>3}style="display:none;"{/if} class="_all _{$service.slug}">
                                        <td>
                                            <a href="{$system_url}clientarea/services/{$service.slug}/{$service.id}/" class="bold">
                                                <strong>{$service.name}</strong><br />
                                                <span>{$service.domain}</span>
                                            </a>
                                        </td>
                                        <td><span class="label label-{$service.status}">{$lang[$service.status]}</span></td>
                                        <td><span class="visible-desktop">{$service.total|price:$currency}</span></td>
                                        <td><span class="visible-desktop">{$lang[$service.billingcycle]}</span></td>
                                        <td><span class="hidden-phone-portrait">
                                            {if $service.next_due && $service.next_due!='0000-00-00'}
                                                <small>{$lang.nextdue}</small><br />
                                                <span>{$service.next_due|dateformat:$date_format}</span>
                                            {else} - 
                                            {/if}
                                            </span>
                                        </td>
                                        <td >
                                            <a href="{$system_url}clientarea/services/{$service.slug}/{$service.id}/"><i class="icon-cog color-grey"></i></a>
                                        </td>
                                    </tr>
                                {/foreach}
                            {else}
                                <tr class="_all">
                                    <td colspan="6">
                                        <a href="{$system_url}cart/" class="btn btn-primary pull-left"><i class="icon-shopping-cart icon-large"></i> &nbsp; {$lang.ordermore}</a>
                                    </td>
                                </tr>
                            {/if}
                        </tbody>
                    </table>
                </div>
                <div class="table-pages">
                    <div class="pull-left">
                        <a href="#board-services" class="slide-left"  title="preview">
                            <span class="entypo">&#59237;</span>
                        </a>
                        <span class="page-current"></span> {$lang.pageof} <span class="page-total"></span>
                        <a href="#board-services" class="slide-right" title="next">
                            <span class="entypo">&#59238;</span>
                        </a>
                    </div>
                </div>
            </div><!--/slides wrapper --> 
        </div> <!--/tabbable --> 
    </div><!--/row-->

    <div class="row-fluid">
        {if $openedtickets}
            <div class="{if $dueinvoices}col-6{else}col-12{/if}">
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {$lang.myhistory}
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <input type="hidden" id="currentpage" value="0" />
        <table class="text-center m-datatable table table-striped- table-bordered table-hover table-checkable" id="html_table"
            width="100%">
            <thead>
                <tr>
                    <th class="col-3" title="{$lang.date}" data-field="date">{$lang.subject}</th>
                    <th class="col-3" title="{$lang.date_sent}" data-field="text1">{$lang.status}</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$openedtickets item=lticket name=foo}
                <tr>
                    <td><a href="{$system_url}tickets/view/{$lticket.ticket_number}/">{$lticket.subject}</a></td>
                    <td><span class="label label-{$lticket.status}">{if $lang[$lticket.status]}{$lang[$lticket.status]|upper}{else}{$lticket.status}{/if}</span></td>
                </tr>
                {/foreach}
            </tbody>
        </table>

        <!--end: Datatable -->
    </div>
</div>
            </div><!--/span6-->
        {/if}
        {if $dueinvoices}
            <div class="{if $openedtickets}span6{else}span12{/if}" id="board-invoices">
                <span class="icon icon-Files"></span>
                <h3>{$lang.invoices}</h3>
                <div class="invoices">
                    <div class="slides-wrapper"> 
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{$lang.invoicenum}</th>
                                    <th class="hidden-phone-portrait">{$lang.duedate}</th>
                                </tr>    
                            </thead>
                        </table>
                        <div class="spacing">    
                            <table class="table">
                                <tbody>
                                    {foreach from=$dueinvoices item=invoice name=foo}
                                        <tr>
                                            <td>
                                                <a href="{$system_url}clientarea/invoice/{$invoice.id}/" target="_blank">
                                                    <span class="label label-Unpaid">{$lang.Unpaid}</span>
                                                    {$lang.invoice|capitalize} #{$invoice|@invoice}
                                                </a>
                                            </td>
                                            <td class="hidden-phone-portrait">
                                                {$invoice.duedate|dateformat:$date_format}
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div><!--/space20-->
                        <div class="table_bottom">
                            <div class="pull-left">
                                <a href="#board-invoices" class="slide-left" title="preview"><span class="entypo">&#59237;</span></a>
                                <span class="page-current"></span> {$lang.pageof} <span class="page-total"></span>
                                <a href="#board-invoices" class="slide-right" title="next"><span class="entypo">&#59238;</span></a>
                            </div>

                        </div>
                    </div><!--/slides-wrapper-->
                </div> 
            </div><!--/span6-->
        {/if}
    </div>
</div><!-- spacing -->

<div class="clear"></div>

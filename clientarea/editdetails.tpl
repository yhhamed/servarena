
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    {$lang.details}
                                </h3>
                            </div>
                        </div>
                    </div>
        
                    <!--begin::Form-->
                    <form class="m-form m-form--label-align-right" action='' method='post'>
                    <input type="hidden" name="make" value="details" />
                        <div class="m-portlet__body">
                                <div class="m-form__section">
                                    {foreach from=$fields item=field name=floop key=k}
                                    {if $field.field_type=='Password'}{continue}{/if}
                                    <div {if $field.type=='Company' && $fields.type}class="iscomp form-group m-form__group row" style="{if !$client.company || $client.type=='Private'}display:none{/if}"{elseif $field.type=='Private' && $fields.type}class="ispr form-group m-form__group row" style="{if $client.company=='1'}display:none{/if}"{else}class="form-group m-form__group row"{/if}>
                                        <label class="col-lg-2 col-form-label">
                                        {if $k=='type'}
                                                                        {$lang.clacctype}
                                                                    {elseif $field.options & 1}
                                                                        {if $lang[$k]}
                                                                            {$lang[$k]}
                                                                        {else}
                                                                            {$field.name}
                                                                        {/if}
                                                                    {else}
                                                                        {$field.name}
                                                                    {/if}
                                                                    {if $field.options & 2}
                                                                        *
                                                                    {/if}
                                        </label>
            <div class="col-lg-6">
                {if $k=='type'}
                                            <select class="form-control m-input"  name="type" onchange="{literal}if ($(this).val() == 'Private') {
                                                        $('.iscomp').hide();
                                                        $('.ispr').show();
                                                    } else {
                                                        $('.ispr').hide();
                                                        $('.iscomp').show();
                                                    }{/literal}">
                                                <option value="Private" {if $client.company=='0'}selected="selected"{/if}>{$lang.Private}</option>
                                                <option value="Company" {if $client.company=='1'}selected="selected"{/if}>{$lang.Company}</option>
                                            </select>
                                        {elseif $k=='country'}
                                            {if !($field.options & 8)}
                                                {foreach from=$countries key=k item=v} 
                                                    {if $k==$client.country}
                                                        {$v}
                                                    {/if}
                                                {/foreach}
                                            {else}
                                                <select name="country" class="form-control m-input">
                                                    {foreach from=$countries key=k item=v}
                                                        <option value="{$k}" {if $k==$client.country  || (!$client.country && $k==$defaultCountry)} selected="selected"{/if}>{$v}</option>
                                                    {/foreach} 
                                                {/if}
                                            </select>
                                        {else}
                                            {if !($field.options & 8)}
                                                {if $field.field_type=='Password'}
                                                {elseif $field.field_type=='Check'}
                                                    {foreach from=$field.default_value item=fa}
                                                        {if in_array($fa,$client[$k])}
                                                            {$fa}<br/>
                                                        {/if}
                                                    {/foreach}
        
                                                {else}
                                                    {$client[$k]} <input type="hidden" value="{$client[$k]}" name="{$k}"/>
        
                                                {/if}
        
                                            {else}
                                                {if $field.field_type=='Input'}
                                                    <input type="text" value="{$client[$k]}" name="{$k}" class="form-control m-input"/>
                                                {elseif $field.field_type=='Password'}
                                                {elseif $field.field_type=='Select'}
                                                    <select name="{$k}" class="form-control m-input">
                                                        {foreach from=$field.default_value item=fa}
                                                            <option {if $client[$k]==$fa}selected="selected"{/if}>{$fa}</option>
                                                        {/foreach}
                                                    </select>
                                                {elseif $field.field_type=='Check'}
                                                    {foreach from=$field.default_value item=fa}
                                                        <input type="checkbox" name="{$k}[{$fa|escape}]" value="1" {if in_array($fa,$client[$k])}checked="checked"{/if} />
                                                        {$fa}<br />
                                                    {/foreach}
                                                {/if}
                                            {/if}
                                        {/if}
            </div>
        </div>
        {/foreach}
                                </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary ">{$lang.savechanges}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {securitytoken}
                    </form>
        
                    <!--end::Form-->
                </div>

                <!--end::Portlet-->            
            </div>
        </div>
    </div>

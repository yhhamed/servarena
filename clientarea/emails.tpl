
<div class="m-content">

    {if $email}
        <h3>{$email.subject}</h3>
        <p class="pull-up"><i class="icon-time"></i> {$email.date|dateformat:$date_format}</p>
        <div class="m-content">{$email.message|httptohref}</div>
    {else}

						<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
												{$lang.emhistory}
										</h3>
									</div>
								</div>
							</div>
							<div class="m-portlet__body">

								<!--begin: Search Form -->
								<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
									<div class="row align-items-center">
										<div class="col-xl-8 order-2 order-xl-1">
											<div class="form-group m-form__group row align-items-center">

												<div class="col-md-4">
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input" placeholder="{$lang.searchemails}" id="generalSearch">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="la la-search"></i></span>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<!--end: Search Form -->

								<!--begin: Datatable -->
                                <input type="hidden" id="currentpage" value="0" />
								<table class="m-datatable" id="html_table" width="100%">
									<thead>
										<tr>
											<th title="{$lang.subject}" data-field="subject">{$lang.subject}</th>
											<th title="{$lang.date_sent}" data-field="date">{$lang.date_sent}</th>
										</tr>
									</thead>
									<tbody>
										{include file='ajax/ajax.emails.tpl'}									
									</tbody>
								</table>

								<!--end: Datatable -->
							</div>
						</div>
					</div>
    {/if}
</div>
{literal}
<script>
var DatatableHtmlTableDemo = function() {
	//== Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.m-datatable').mDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'subject',
					type: 'text',
				},
			],
		});

		$('#m_form_status').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Status');
		});

		$('#m_form_type').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Type');
		});

		$('#m_form_status, #m_form_type').selectpicker();

	};
	return {
		//== Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();
jQuery(document).ready(function() {
	DatatableHtmlTableDemo.init();
});
</script>
{/literal}
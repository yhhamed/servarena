
<div class="m-content">

						<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
												{$lang.myhistory}
										</h3>
									</div>
								</div>
							</div>
							<div class="m-portlet__body">
								<!--begin: Datatable -->
                                <input type="hidden" id="currentpage" value="0" />
								<table  class="text-center m-datatable table table-striped- table-bordered table-hover table-checkable"  id="html_table" width="100%">
									<thead>
										<tr>
											<th class="col-3" title="{$lang.date}" data-field="date">{$lang.date}</th>
											<th class="col-3" title="{$lang.date_sent}" data-field="text1">{$lang.Description}</th>
                                            <th class="col-3" title="{$lang.status}" data-field="text2">{$lang.status}</th>
                                            <th class="col-3" title="{$lang.details}" data-field="text3">{$lang.details}</th>
										</tr>
									</thead>
									<tbody>
										{include file='ajax/ajax.history.tpl'}									
									</tbody>
								</table>

								<!--end: Datatable -->
							</div>
						</div>
					</div>

</div>

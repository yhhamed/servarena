
    <div class="m-content">
        <div class="alert alert-info" role="alert">
            <label>{$lang.examplerules}:</label>
            <div class="col-md-6 mb-3">
                <ul class="rules-list">
                    <li><strong>all</strong> - {$lang.keywordmatchingall}</li>
                    <li><strong>xxx.xxx.xxx.xxx</strong> - {$lang.singleiprule}</li>
                    <li><strong>xxx.xxx.xxx.xxx/M</strong> - {$lang.ipmaskrule}</li>
                    <li>{$lang.ipmaskruledoted}</li>
                </ul>
            </div>
            <div class="col-md-6 mb-3">
                <ul class="rules-list rules-m">
                    <li>{$lang.ruleexample1}</li>
                    <li>{$lang.ruleexample2}</li>
                </ul>
            </div>
        </div>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {$lang.ipaccess}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <span>{$lang.currentipadd} {$yourip}</span>
                <form class="form-style" method="post" action="">
                    <input type="hidden" name="make" value="addrule" />
                    <label for="ipsubnet">{$lang.ipsubnet}</label>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <input name="rule" type="text" class="form-control m-input" placeholder="eg.  {$yourip}">
                        </div>
                        <div class="col-md-6 mb-3">
                            <button type="submit" class="btn btn-primary">{$lang.addipsubnet}</button>
                        </div>
                    </div>
                    {securitytoken}
                </form>
                <table class="text-center m-datatable table table-striped-  table-hover table-checkable">
                    <thead>
                        <tr>
                            <th>{$lang.ipsubnet}</th>
                            <th>{$lang.action}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$rules item=rule name=rules}
                        <tr>
                            <td>
                                {if $rule.rule == 'all'}
                                {$lang.allaccess} -
                                {else}
                                {$rule.rule} -
                                {/if}
                            </td>
                            <td>
                                <a class="deleteico" href="{$system_url}{$cmd}/{$action}/&make=delrule&id={$rule.id}">{$lang.delete}</a>
                            </td>
                        </tr>
                        {foreachelse}
                        <tr>
                            <td>{$lang.norules} - {$lang.allaccess}</td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

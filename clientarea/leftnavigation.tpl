<ul class="nav nav-list sh-menu pull-left span3">
    {foreach from=$sitemap.account.pages item=link key=type name=leftnav}
        {if $link.visible && $link.subnav}
            <li class="{if $smarty.foreach.leftnav.first}no-border {/if}{if $link.active}active{/if}">
                <a href="{$link.url}">

                        {if $type == 'details'}
                            <span class="entypo">&#128100;</span>
                        {elseif $type == 'profilepassword' || $type == 'password'}
                            <span class="entypo">&#128274;</span>
                        {elseif $type == 'ccard' || $type == 'ach'}
                            <span class="entypo">&#128179;</span>
                        {elseif $type == 'ipaccess'}
                            <span class="entypo">&#127758;</span>
                        {elseif $type == 'addfunds'}
                            <span class="entypo">&#8862;</span>
                        {elseif $type == 'profiles'}
                            <span class="entypo">&#59170;</span>
                        {else}
                            <span class="entypo">&#9881;️</span>
                        {/if}
                    <p class="sh-text">
                        {if $lang[$link.alt_lang]}{$lang[$link.alt_lang]}
                        {else}{$lang[$link.lang]}
                        {/if}
                    </p>
                </a>
            </li>
        {/if}
    {/foreach}
</ul>


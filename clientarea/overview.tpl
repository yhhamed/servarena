<div class="spacing">
    <div class="shared-hosting-example">
        {include file="clientarea/leftnavigation.tpl"}
        <div class="sh-container span9">

        <h4>{$lang.overview}</h4>

        <div class="spacing">
            <div >{$lang.overview_d}</div>
            <table class="table table-striped" style="table-layout: fixed">
                <thead>
                    <tr>
                        <th>{$lang.information_type}</th>
                        <th>{$lang.clientdata}</th>
                        <th>{$lang.purpose}</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$fields item=field key=k}
                        <tr>
                            <td>
                                {if $field.options & 1}{if $lang[$k]}{$lang[$k]}{else}{$field.name}{/if}
                                {else}{$field.name}
                                {/if}
                            </td>
                            <td>
                                {if $field.field_type=='Input'}{$client[$k]}
                                {elseif $field.field_type=='Password'}
                                {elseif $field.field_type=='Select'}
                                    {foreach from=$field.default_value item=fa}
                                        {if $client[$k]==$fa}{$fa}{/if}
                                    {/foreach}
                                {elseif $field.field_type=='Check'}
                                    {foreach from=$field.default_value item=fa}
                                        {if in_array($fa,$client[$k])}{$fa}<br/>{/if}
                                    {/foreach}
                                {/if}
                            </td>
                            <td>
                                {if $field.options & 4}
                                    {$lang.billing}
                                {else}
                                    {$field.description|escape}
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>


            <div class="form-actions">
                <p>{$lang.download_overview_json}</p>
                <a href="{$system_url}clientarea/{$action}/&download" class="btn btn-info">{$lang.download}</a>
            </div>
            {if $canDelete}
            <hr/>
            <p>
                <a href="{$system_url}clientarea/delete" class="btn btn-danger">{$lang.delete_account}</a>
            </p>
            {/if}
            {securitytoken}
        </div>

    </div>
</div>
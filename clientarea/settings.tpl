<div class="spacing">
    <div class="shared-hosting-example">
        {include file='clientarea/leftnavigation.tpl'}
        <div class="sh-container span9">
        <h4>{$lang.Settings}</h4>
            <div class="spacing">
                <div>{$lang.Settings_p}</div>
        <br />
        <div class="p19">
            <form class="form-horizontal" method="post">
                {include file="../common/tpl/settings.tpl"}
                <center><button type="submit" class="btn btn-primary btn-large" name="submit">{$lang.savechanges}</button></center>
                {securitytoken}
            </form>
        </div>

    </div>
</div>
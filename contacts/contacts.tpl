
<div class="m-content">
        <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {$lang.profiles}
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                    <a href="{$system_url}profiles/add/" class="m-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air"><i class="flaticon-user-add"></i> {$lang.addnewprofile}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{$lang.name}</th>
                                        <th>{$lang.email}</th>
                                        <th>{$lang.lastlogin}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach from=$profiles item=p name=ff}
                                    <tr>
                                        <td><span>{$p.firstname} {$p.lastname}</span></td>
                                        <td><span>{$p.email}</span>></td>
                                        <td>
                                            {if !$p.lastlogin|dateformat:$date_format} -
                                            {else}{$p.lastlogin|dateformat:$date_format}
                                            {/if}
                                        </td>
                                        <td>
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--align-push"
                                                m-dropdown-toggle="hover" aria-expanded="true">
                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link">
                                                    <i class="la la-ellipsis-v"></i>
                                                </a>
                                                <div class="m-dropdown__wrapper">
                                                    <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                                    <div class="m-dropdown__inner">
                                                        <div class="m-dropdown__body">
                                                            <div class="m-dropdown__content">
                                                                <ul class="m-nav">
                                                                    <li class="m-nav__section m-nav__section--first">
                                                                        <span class="m-nav__section-text">Quick Actions</span>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="{$ca_url}profiles/edit/{$p.id}/" class="m-nav__link">
                                                                            <i class="m-nav__link-icon flaticon-edit"></i>
                                                                            <span class="m-nav__link-text">{$lang.editcontact}</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="{$ca_url}profiles/loginascontact/{$p.id}/" class="m-nav__link">
                                                                            <i class="m-nav__link-icon  flaticon-refresh"></i>
                                                                            <span class="m-nav__link-text">{$lang.loginascontact}</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="{$ca_url}profiles/&do=delete&id={$p.id}&security_token={$security_token}" class="m-nav__link" onclick="return confirm('{$lang.areyousuredelete}');">
                                                                            <i class="m-nav__link-icon   flaticon-delete"></i>
                                                                            <span class="m-nav__link-text">{$lang.delete}</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                {foreachelse}
                                    <tr>
                                        <td>{$lang.nothing}</td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>
</div>
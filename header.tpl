<!DOCTYPE html>
<html lang='{if $language == "arabic"}ar{else}en{/if}'>

<head>
	<meta charset="utf-8" />
	<title>{$hb}{if $pagetitle}{$pagetitle} -{/if} {$business_name}</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		{literal}<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700","Cairo:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>{/literal}
		
	{if $language == "arabic"}
	<link href="{$template_dir}assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />
	<link href="{$template_dir}assets/demo/demo7/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />
	<link href="{$template_dir}assets/vendors/custom/datatables/datatables.bundle.rtl.css" rel="stylesheet" type="text/css" />
	{/if}

	{if $language == "english"}
	<link href="{$template_dir}assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{$template_dir}assets/demo/demo7/base/style.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{$template_dir}assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	{/if}
	{userheader}
	<link href="{$template_dir}assets/demo/demo7/base/custom.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="{$template_dir}assets/demo/demo7/media/img/logo/favicon.ico" />
		<!--begin::Global Theme Bundle -->
		<script src="{$template_dir}assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="{$template_dir}assets/demo/demo7/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="{$template_dir}assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		<!--end::Global Theme Bundle -->

		<!--begin::Page Scripts -->
		<script src="{$template_dir}assets/app/js/dashboard.js" type="text/javascript"></script>

</head>
	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">

						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="{$system_url}/" class="m-brand__logo-wrapper">
										<img alt="" src="{$template_dir}assets/demo/demo7/media/img/logo/logo.png" />
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">

									<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>

									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
						
							<!-- BEGIN: Topbar -->
							
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item">
											<a href="{$system_url}cart/" class="m-nav__link ">
												<span class="m-nav__link-icon">
													<span class="m-nav__link-icon-wrapper"><i class="flaticon-cart"></i></span>
												</span>
											</a>
										</li>
										{if $logged=='1'}
										<li class="m-nav__item ">
											<a href="{$system_url}tickets/" class="m-nav__link ">
												<span class="m-nav__link-icon">
													<span class="m-nav__link-icon-wrapper"><i class="flaticon-chat-1 "></i></span>
												</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="{$system_url}clientarea/invoices/" class="m-nav__link ">
												<span class="m-nav__link-icon">
													<span class="m-nav__link-icon-wrapper"><i class="flaticon-interface-11"></i></span>
												</span>
											</a>
										</li>
										{/if}
										
										<li class="m-nav__item">
											<a href="{$system_url}clientarea{if $logged=='1'}/details/{/if}" class="m-nav__link ">
												<span class="m-nav__link-icon">
													<span class="m-nav__link-icon-wrapper"><i class="flaticon-settings-1"></i></span>
												</span>
											</a>
										</li>
										<li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--small m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click" aria-expanded="true" m-dropdown-persistent="1">
											<a href="#" class="m-nav__link m-dropdown__toggle ">
												<span class="m-nav__link-icon">
													<span class="m-nav__link-icon-wrapper"><i class=" flaticon-earth-globe"></i></span>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
															{foreach from=$languages item=ling}
																<li class="m-nav__item"><a href="{$system_url}{$cmd}&action={$action}&languagechange={$ling|capitalize}">{$lang[$ling]|capitalize}</a></li>
															{/foreach}
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--small m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click" aria-expanded="true">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-icon m-topbar__usericon">
													<span class="m-nav__link-icon-wrapper"><i class=" flaticon-user"></i></span>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
															{if $logged!='1'}
																<li  class="m-nav__item"><a href="{$system_url}signup/">{$lang.createaccount}</a></li>
																<li  class="m-nav__item"><a href="{$system_url}login/">{$lang.login}</a></li>
																{else}
																<li  class="m-nav__item"><a href="{$system_url}clientarea/details/">{$lang.manageaccount}</a></li>
																<li  class="m-nav__item{if $adminlogged} border-bottom{/if}"><a href="?action=logout">{$lang.logout}</a></li>
																{/if}
																{if $adminlogged}
																
																<li  class="m-nav__item"><a  href="{$admin_url}/index.php{if $login.id}?cmd=clients&amp;action=show&amp;id={$login.id}{/if}">{$lang.adminreturn}</a></li>
															
															{/if}
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>

							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>

			<!-- END: Header -->
			
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">

					<!-- BEGIN: Brand -->
					<div class="m-brand  m-brand--skin-light ">
						<a href="{$system_url}" class="m-brand__logo">
							<img alt="" src="{$template_dir}assets/demo/demo7/media/img/logo/logo.png" />
						</a>
					</div>

					<!-- END: Brand -->

					<!-- BEGIN: Aside Menu -->
					<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " data-menu-vertical="true" m-menu-scrollable="true" m-menu-dropdown-timeout="500">
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							{if $logged=='1'}
								 <!-- Done -->    
								{include file='menus/menu.main.logged.tpl'}

							{else}
								<!-- Done --> 
								{include file='menus/menu.main.notlogged.tpl'}

							{/if}    
						</ul>
					</div>

					<!-- END: Aside Menu -->
				</div>
				
    
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
				
					{if $error}
					<div class="alert alert-warning" role="alert">
							{foreach from=$error item=blads}<span>{$blads}</span>{/foreach}
					</div>
					{/if}
					{if $info}
					<div class="alert alert-info" role="alert">
							{foreach from=$info item=infos}<span>{$infos}</span>{/foreach}
					</div>
					{/if}

<div class="m-menu__submenu m-menu__item--submenu-fullheight">
    <span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">{$lang.account}</span></span></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/details/" class="m-menu__link "><i class="m-menu__link-icon  flaticon-edit-1"></i><span class="m-menu__link-text">{$lang.details}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/password/" class="m-menu__link "><i class="m-menu__link-icon  flaticon-visible "></i><span class="m-menu__link-text">{$lang.changepass}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}profiles" class="m-menu__link "><i class="m-menu__link-icon  flaticon-user-settings"></i><span class="m-menu__link-text">{$lang.managecontacts}</span></a></li>
            {if $enableFeatures.security=='on'}<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/ipaccess/" class="m-menu__link "><i class="m-menu__link-icon  flaticon-security"></i><span class="m-menu__link-text">{$lang.security}</span></a></li>{/if}
            {if $enableFeatures.deposit!='off'}<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/addfunds/" class="m-menu__link "><i class="m-menu__link-icon   flaticon-coins "></i><span class="m-menu__link-text">{$lang.addfunds}</span></a></li>{/if}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/invoices/" class="m-menu__link "><i class="m-menu__link-icon   flaticon-interface-11"></i><span class="m-menu__link-text">{$lang.invoices}</span></a></li>
            {if $enableCCards}<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/ccard/" class="m-menu__link "><i class="m-menu__link-icon   flaticon-plus"></i><span class="m-menu__link-text">{$lang.ccard}</span></a></li>{/if}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/emails/" class="m-menu__link "><i class="m-menu__link-icon   flaticon-mail"></i><span class="m-menu__link-text">{$lang.emails}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}clientarea/history/" class="m-menu__link "><i class="m-menu__link-icon  flaticon-layer "></i><span class="m-menu__link-text">{$lang.logs}</span></a></li>
        </ul>
</div>





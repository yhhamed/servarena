<div class="m-menu__submenu ">
    <span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">{$lang.affiliates}</span></span></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}affiliates/" class="m-menu__link "><i class="m-menu__link-icon la   fa-dot-circle"></i><span class="m-menu__link-text">{$lang.summary}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}affiliates/commissions/" class="m-menu__link "><i class="m-menu__link-icon la   fa-dot-circle"></i><span class="m-menu__link-text">{$lang.mycommissions}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}affiliates/payouts/" class="m-menu__link "><i class="m-menu__link-icon la   fa-dot-circle"></i><span class="m-menu__link-text">{$lang.payouts}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}affiliates/vouchers/" class="m-menu__link "><i class="m-menu__link-icon la   fa-dot-circle"></i><span class="m-menu__link-text">{$lang.managevouchers}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}affiliates/addvoucher/" class="m-menu__link "><i class="m-menu__link-icon la   fa-dot-circle"></i><span class="m-menu__link-text">{$lang.createnewvoucher}</span></a></li>
        </ul>
</div>
<div class="m-menu__submenu ">
    <span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">{$lang.order}</span></span></li>
		{foreach from=$orderpages item=op}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}cart/{$op.slug}/" class="m-menu__link "><i class="m-menu__link-icon la   fa-dot-circle"></i><span class="m-menu__link-text">{$op.name}</span></a></li>
		{/foreach}
        </ul>
</div>
<div class="m-menu__submenu ">
    <span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">

    {foreach from=$offer item=offe}
        {if $offe.total>0}
        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}clientarea/services/{$offe.slug}/" class="m-menu__link "><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">{$offe.name} </span>      <span class="m-menu__link-badge"><span class="m-badge m-badge--focus">{$offe.total}</span></span>  </span></span></a></li>
            {assign value=true var=hasservicedrop}
        {/if}

    {/foreach}

    {if $offer_domains}
        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}clientarea/domains/" class="m-menu__link "><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">{$lang.domains} </span>      <span class="m-menu__link-badge"><span class="m-badge m-badge--focus">{$offer_domains}</span></span>  </span></span></a></li>
    {/if}

    {if $offer_domains==0 && !$hasservicedrop}
    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}cart/" class="m-menu__link "><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">{$lang.noservicesyet} </span>      <span class="m-menu__link-badge"><span class="m-badge m-badge--danger">0</span></span>  </span></span></a></li>


    {/if}

        </ul>
</div>


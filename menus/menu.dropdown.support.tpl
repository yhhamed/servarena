<div class="m-menu__submenu ">
    <span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">{$lang.support}</span></span></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}tickets/new/" class="m-menu__link "><i class="m-menu__link-bullet   la-dot-circle-o "><span></span></i><span class="m-menu__link-text">{$lang.openticket}</span></a></li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}tickets/" class="m-menu__link "><i class="m-menu__link-bullet   la-dot-circle-o "><span></span></i><span class="m-menu__link-text">{$lang.ticketarchive}</span></a></li>
        {if $enableFeatures.kb!='off'}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}knowledgebase/" class="m-menu__link "><i class="m-menu__link-bullet   la-dot-circle-o "><span></span></i><span class="m-menu__link-text">{$lang.vsarticles}</span></a></li>
        {/if}
        {if $enableFeatures.downloads!='off'}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}downloads/" class="m-menu__link "><i class="m-menu__link-bullet   la-dot-circle-o "><span></span></i><span class="m-menu__link-text">{$lang.browsedownloads}</span></a></li>
        {/if}
        {if $enableFeatures.downloads!='off'}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}chat/" class="m-menu__link "><i class="m-menu__link-bullet   la-dot-circle-o "><span></span></i><span class="m-menu__link-text">{$lang.chat}</span></a></li>
        {/if}
        {if $enableFeatures.downloads!='off'}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a  href="{$system_url}netstat/" class="m-menu__link "><i class="m-menu__link-bullet   la-dot-circle-o "><span></span></i><span class="m-menu__link-text">{$lang.netstat}</span></a></li>
        {/if}
        </ul>
</div>






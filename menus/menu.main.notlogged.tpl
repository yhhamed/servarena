<li class="m-menu__item m-menu__item--submenu m-menu__item--submenu-fullheight">
    <a href="{$system_url}" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon  flaticon-home-2"></i>
        <span class="m-menu__link-text">{$lang.dashboard}</span><i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
</li>

<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1">
<a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon   flaticon-cart "></i>
    <span class="m-menu__link-text">{$lang.order}</span><i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
    {include file='menus/menu.dropdown.cart.tpl'}
</li>

<li class="m-menu__item m-menu__item--submenu m-menu__item--submenu-fullheight">
    <a href="{$system_url}clientarea/" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon  flaticon-settings "></i>
        <span class="m-menu__link-text">{$lang.clientarea}</span><i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
</li>

<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1">
<a href="{$system_url}{if $enableFeatures.kb!='off'}knowledgebase/{else}tickets/new/{/if}" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon    flaticon-questions-circular-button"></i>
    <span class="m-menu__link-text">{$lang.support}</span><i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
    {include file='menus/menu.dropdown.support.tpl'}
</li>

{if $enableFeatures.affiliates!='off'}
    <li class="m-menu__item m-menu__item--submenu m-menu__item--submenu-fullheight">
        <a href="{$system_url}affiliates/" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon   flaticon-coins"></i>
            <span class="m-menu__link-text">{$lang.affiliates}</span><i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
    </li>
{/if}



{foreach from=$HBaddons.client_mainmenu item=ad}
    <li class="m-menu__item m-menu__item--submenu m-menu__item--submenu-fullheight">
        <a href="{$system_url}{$ad.link}/" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-tool-1 "></i>
            <span class="m-menu__link-text">{$ad.name}</span><i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
    </li>
{/foreach}



{if $infopages}

    <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1">
    <a href="#" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-layer"></i>
        <span class="m-menu__link-text">{$lang.moreinfo}</span><i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
        <div class="m-menu__submenu ">
    <span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">{$lang.support}</span></span></li>
        {foreach from=$infopages item=paged}
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{$system_url}page/{$paged.url}/" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">{$paged.title}</span></a></li>
        {/foreach}
        </ul>
        </div>
    </li>
{/if}
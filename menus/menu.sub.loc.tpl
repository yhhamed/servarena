<!-- Breadcrumbs -->

{if $inside || !(($action == 'services' && $service) || ($action == 'domains' && $details) || ($cmd=='clientarea' && $action=='default')) }
<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
<ul class="m-subheader__breadcrumbs m-nav m-nav--inline {$action} {if $inside}inside-breadcrumb{/if}">

    <li class="m-nav__item">

        <i class=" fa-home"></i></li><li class="m-nav__separator">-</li>

    </li>

    {if $logged=='1' && !($cmd == 'clientarea' && $action == 'default')}

        <li class="m-nav__item">

            <a href="{$ca_url}">{$lang.dashboard}</a></li><li class="m-nav__separator">-</li>

        </li>

    {/if}

    {if $cmd == 'clientarea' && $action == 'default'}

        <li class="m-nav__item">

            {$lang.dashboard}

        </li>

    {elseif $cmd == 'cart'}

        {if $step!=5 && $step>0}

            <li class="m-nav__item">

                <a href="{$ca_url}cart/">{$lang.order}</a>

                </li><li class="m-nav__separator">-</li>

            </li>

            {if $step>2 && (!$cart_contents[2] || $cart_contents[2][0].action == 'hostname')}

                {assign var='pclass' value='asw3'}

            {elseif $step==1 || ($cart_contents[2] && $cart_contents[2][0].action!='own')} 

                {assign var='pclass' value='asw5'}

            {elseif $step>1 && $cart_contents[2][0].action=='own'} 

                {assign var='pclass' value='asw4'}

            {/if}



            {if $pclass!='asw3'}		

                <li class="m-nav__item"{if $step<2}class="m-nav__item"{/if}>			

                    {if $step<=1}{$lang.mydomains}

                    {else}<a href="{$ca_url}cart/&step=1">{$lang.mydomains}</a>

                    {/if}

                    </li><li class="m-nav__separator">-</li>

                </li>

            {elseif $pclass=='asw3'}

                <li class="m-nav__item"{if $step<2}class="m-nav__item"{/if}>		

                    {if $step<=3}{$lang.productconfig}

                    {else}<a href="{$ca_url}cart/&step=3">{$lang.productconfig}</a>

                    {/if}

                    </li><li class="m-nav__separator">-</li>

                </li>

            {/if}



            {if $pclass=='asw5'}	

                <li class="m-nav__item"{if $step==2}class="m-nav__item"{/if}>				

                    {if $step<=2}{$lang.productconfig2}

                    {else}<a href="{$ca_url}cart/&step=2">{$lang.productconfig2}</a>

                    {/if}

                    </li><li class="m-nav__separator">-</li>

                </li>

            {elseif $pclass=='asw4'}			

                <li class="m-nav__item"{if $step==3}class="m-nav__item"{/if}>				

                    {if $step<=3}{$lang.productconfig}

                    {else}<a href="{$ca_url}cart/&step=3">{$lang.productconfig}</a>

                    {/if}

                    </li><li class="m-nav__separator">-</li>

                </li>						

            {/if}



            {if $pclass=='asw5'}	

                <li class="m-nav__item"{if $step==3}class="m-nav__item"{/if}>				

                    {if $step<=3}{$lang.productconfig}

                    {else}<a href="{$ca_url}cart/&step=3">{$lang.productconfig}</a>

                    {/if}

                    </li><li class="m-nav__separator">-</li>

                </li>						

            {/if}

            <li class="m-nav__item"{if $step==4}class="m-nav__item"{/if}>

                {if $step<=4}{$lang.ordersummary}

                {else}<a href="{$ca_url}cart/&step=4">{$lang.ordersummary}</a>

                {/if}

                </li><li class="m-nav__separator">-</li>

            </li>

            <li class="m-nav__item">

                {$lang.checkout}

            </li>

        {else}

            <li class="m-nav__item">

                {$lang.order}

            </li>

        {/if}



    {elseif $cmd == 'clientarea' && ( $action == 'service' || $action == 'services' || $action == 'domains')}

        <li class="m-nav__item">

            <a href="{$ca_url}clientarea/">{$lang.clientarea}</a>

            </li><li class="m-nav__separator">-</li>

        </li>

    {elseif $cmd == 'clientarea' && !( $action == 'service' || $action == 'services' || $action == 'domains' || $action == 'cancel') && $action != 'default'}

        <li class="m-nav__item">

            <a href="{$ca_url}clientarea/">{$lang.account}</a>

            </li><li class="m-nav__separator">-</li>

        </li>

    {elseif $cmd == 'support' || $cmd == 'tickets' || $cmd == 'downloads' || $cmd == 'knowledgebase'} 

        <li class="m-nav__item"{if $cmd == 'support'}class="m-nav__item"{/if}>

            {if $cmd != 'support'}

                <a href="{$ca_url}support/">{$lang.support}</a>

                </li><li class="m-nav__separator">-</li>

            {else}

                {$lang.support}

            {/if}

        </li>

    {elseif $cmd == 'netstat'}

        <li class="m-nav__item">

            <a href="{$ca_url}support/">{$lang.support}</a>

        </li>

        <li class="m-nav__item">

            {$lang.netstat}

        </li>

    {elseif $cmd == 'profiles'}

        <li class="m-nav__item">

            <a href="{$ca_url}clientarea/">{$lang.account}</a>

            </li><li class="m-nav__separator">-</li>

        </li>

        <li class="m-nav__item"{if $action!='add' && $action!='edit'}class="m-nav__item"{/if}>

            {if $action!='add' && $action!='edit'}

                {$lang.profiles}

            {else}

                <a href="{$ca_url}{$cmd}/">{$lang.profiles}</a>

            {/if}

        </li>

    {elseif $cmd == 'affiliates'}

        <li class="m-nav__item"{if $action == 'default'}class="m-nav__item"{/if}>

            {if $action != 'default'}

                <a href="{$ca_url}affiliates/">{$lang.affiliates}</a>

                </li><li class="m-nav__separator">-</li>

            {else}

                {$lang.affiliates}

            {/if}

        </li>

    {else}

        <li class="m-nav__item">

            {if $pagetitle}{$pagetitle}

            {elseif $lang.$cmd}{$lang.$cmd}

            {else}{$cmd}

            {/if}

        </li>

    {/if}



    {if $cmd == 'clientarea' && ( $action == 'service' || $action == 'services' || $action == 'domains')}

        {if $action == 'service' || $action == 'services'}

            {foreach from=$offer item=o}

                {if $cid==$o.id || $service.category_id==$o.id}

                    <li class="m-nav__item"{if !$service}class="m-nav__item"{/if}>

                        {if $service}

                            <a href="{$ca_url}clientarea/services/{$o.slug}/"  >{$o.name}</a>

                            </li><li class="m-nav__separator">-</li>

                        {else}{$o.name}

                        {/if}

                    </li>

                    {break}

                {/if}

            {/foreach}

            {if $service}

                <li class="m-nav__item"{if !$widget && !$domain.domain}class="m-nav__item"{/if}>

                    {if $widget || $domain.domain}

                        <a href="{$ca_url}clientarea/services/{$service.slug}/{$service.id}"  >{$service.name}</a>

                        </li><li class="m-nav__separator">-</li>

                    {else}{$service.name}

                    {/if}

                </li>

            {/if}

            {if $domain.domain}

                <li class="m-nav__item">

                    {$domain.domain}

                </li>

            {/if}

        {else}

            <li class="m-nav__item"{if !$details && !$widget}class="m-nav__item"{/if}>

                {if $details || $widget}

                    <a href="{$ca_url}clientarea/domains/"  >{$lang.domains}</a>

                    </li><li class="m-nav__separator">-</li>

                {else}{$lang.domains}

                {/if}

            </li>

            {if $details}

                <li class="m-nav__item"{if !$widget}class="m-nav__item"{/if}>

                    {if $widget}

                        <a href="{$ca_url}clientarea/domains/{$details.id}/{$details.name}"  >{$details.name}</a>

                        </li><li class="m-nav__separator">-</li>

                    {else}{$details.name}

                    {/if}

                </li>

            {/if}

        {/if}



        {if $widget}

            <li class="m-nav__item">

                {if $lang[$widget.name]}{$lang[$widget.name]}

                {elseif $widget.fullname}{$widget.fullname}

                {else}{$widget.name}

                {/if}

            </li>

        {/if}

    {elseif $cmd == 'clientarea' && !( $action == 'service' || $action == 'services' || $action == 'domains' || $action == 'cancel') && $action != 'default'}

        {if $action=='emails' && $email}

            <li class="m-nav__item">

                <a href="{$ca_url}clientarea/{$lang.$action}/">{$lang.$action}</a>

            </li>

            <li class="m-nav__item">

                {$email.subject|strip|truncate}

            </li>

        {elseif $action=='history' }

            <li class="m-nav__item">

                {$lang.myhistory}

            </li>

        {elseif $action=='profilepassword' }

            <li class="m-nav__item">

                {$lang.changepass}

            </li>

        {elseif $action!='default' && $lang.$action}

            <li class="m-nav__item">

                {$lang.$action}

            </li>

        {else}

            <li class="m-nav__item">

                {$lang.page}

            </li>

        {/if}

    {elseif $cmd == 'tickets' || $cmd == 'downloads' || $cmd == 'knowledgebase'} 

        {if $lang.$cmd}

            <li class="m-nav__item"{if $action=='default' }class="m-nav__item"{/if}>

                {if $action!='default'}

                    <a href="{$ca_url}{$cmd}/" >{$lang.$cmd}</a>

                    </li><li class="m-nav__separator">-</li>

                {else}{$lang.$cmd}

                {/if}

            </li>

        {/if}



        {if $cmd == 'knowledgebase'}

            {if $action!='default' && $lang.$action && !$path}

                <li class="m-nav__item">

                    {$lang.$action}

                </li>

            {/if}



            {foreach from=$path item=p name=kbloc}

                <li class="m-nav__item"{if $smarty.foreach.kbloc.last && !$article}class="m-nav__item"{/if}>

                    {if !$smarty.foreach.kbloc.last || $article}

                        <a href="{$ca_url}knowledgebase/category/{$p.id}/{$p.slug}/">{$p.name}</a> 

                        </li><li class="m-nav__separator">-</li>

                    {else}

                        {$p.name}

                    {/if}

                </li>

            {/foreach}



            {if $article}

                <li class="m-nav__item">

                    {$article.title|truncate:30}

                </li>

            {/if}

        {elseif $cmd == 'downloads'}

            {if $category}

                <li class="m-nav__item">

                    {$category.name}

                </li>

            {/if}

        {else}

            {if $action =='new'}

                <li class="m-nav__item">

                    {$lang.openticket}

                </li>

            {elseif $action=='view'}

                <li class="m-nav__item">

                    {$ticket.subject}

                </li>

            {elseif $action!='default' && $lang.$action}

                <li class="m-nav__item">

                    {$lang.$action}

                </li>

            {/if}

        {/if}



    {elseif $cmd == 'affiliates'}

        {if $action=='commissions'}

            <li class="m-nav__item">

                {$lang.mycommissions}

            </li>

        {elseif $action!='default' && $lang.$action}

            <li class="m-nav__item">

                {$lang.$action}

            </li>

        {/if}

    {elseif $cmd == 'profiles'}

        {if $action=='add'}

            <li class="m-nav__item">

                {$lang.addnewprofile}

            </li>

        {elseif $action=='edit'}

            <li class="m-nav__item">

                {$lang.editcontact}

            </li>

        {/if}

    {/if}



</ul>

</div>
</div>
</div>
{/if}

<!-- End of Breadcrumbs -->
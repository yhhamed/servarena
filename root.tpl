
<div class="m-subheader ">
	<div class="d-flex align-items-center">
 		<div class="mr-auto">
 			<h3 class="m-subheader__title m-subheader__title--separator">{$lang.welcome}</h3>			
							
			 		</div>
  		
	</div>
</div>
<div class="m-content">
<div class="m-portlet">
	<div class="m-portlet__body  m-portlet__body--no-padding">
		<div class="row m-row--no-padding m-row--col-separator-xl">			
			
			
			
		<div class="col-xl-4">
				<!--begin:: Widgets/Revenue Change-->
<div class="m-widget14">
	<a href="{$system_url}cart/">

                <div class="square-box square-box-sky-blue">

                    <div class="middle-cricle">

                        <i class="icon-shopping-cart"></i>

                    </div>

                    <p>{$lang.placeorder|capitalize}</p>

                </div>

            </a>
	
</div>
<!--end:: Widgets/Revenue Change-->			</div><div class="col-xl-4">
				<!--begin:: Widgets/Revenue Change-->
<div class="m-widget14">
	<a href="{$system_url}clientarea/">

                <div class="square-box square-box-sky-blue">

                    <div class="middle-cricle">

                        <i class="icon-shopping-cart"></i>

                    </div>

                    <p>{$lang.clientarea|capitalize}</p>

                </div>

            </a>
	
</div>
<!--end:: Widgets/Revenue Change-->			</div><div class="col-xl-4">
				<!--begin:: Widgets/Revenue Change-->
<div class="m-widget14">
	<a href="{if $logged=='1'}{$system_url}support{elseif $enableFeatures.kb!='off'}{$system_url}knowledgebase{else}{$system_url}tickets/new{/if}/">

                <div class="square-box square-box-sky-blue">

                    <div class="middle-cricle">

                        <i class="icon-shopping-cart"></i>

                    </div>

                    <p>{$lang.support|capitalize}</p>

                </div>

            </a>
	
</div>
<!--end:: Widgets/Revenue Change-->			</div></div>
	</div>
</div>



    
{if $enableFeatures.news!='off' && $annoucements}
<h3>{$lang.annoucements}</h3>
<div class=" row">        
        <!--begin::Portlet-->
        {foreach from=$annoucements item=annoucement name=announ}
        <div class="col-6">
                        <h1>
                            <a href="{$system_url}news/view/{$annoucement.id}/{$annoucement.slug}/" class="roll-link">{$annoucement.title}</a>
                        </h1>
                        <span><i class="icon-time"></i> {$lang.published} <strong>{$annoucement.date|dateformat:$date_format}</strong></span>
            <p>
                {$annoucement.content}
            </p>
        </div>
        {/foreach}
        <!--end::Portlet-->

 
</div>
{/if}

</div>

<div id="tweets"></div>
</div>
</div>
//== Class definition
{ldelim}'{if $action == 'signup'}'{redlim}
var FormControls = function () {
    //== Private functions
    
    var demo1 = function () {
        $( "#signupform" ).validate({
            // define validation rules
            rules: {
                {ldelim}
                '{foreach from=$fields item=field name=floop key=k}'
                {$k}: {
                   ' {if $field.options & 2}'required: true,'{/if}'
                },
                '{/foreach}'
                {redlim}
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
            }
        });       
    }


    return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

jQuery(document).ready(function() {    
    FormControls.init();
});
{ldelim}'{/if}'{redlim}
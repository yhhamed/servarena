{if $service}
    {include file='services/service_details.tpl'}
{else}
    {if $services}
        {if $custom_template}
            <div class="m-content">
                {include file=$custom_template}
            </div>
        {else}
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                    {if $action=='services' && $cid}
                                    {foreach from=$offer item=o}{if $action=='services' && $cid==$o.id}{$o.name}
                                        {/if}
                                    {/foreach}
                                {else}
                                    {$lang[$action]|capitalize}
                                {/if}
                            </h3>
                        </div>
                    </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-form__group m-form__group--inline">
                                            <div class="m-form__label">
                                                <label>Status:</label>
                                            </div>
                                            <div class="m-form__control">
                                                <select class="form-control m-bootstrap-select" id="m_form_status">
                                                    <option value="">{$lang.all}</option>
                                                    <option value="1">{$lang.Active}</option>
                                                    <option value="2">{$lang.Cancelled}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="d-md-none m--margin-bottom-10"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
                    <!--end: Search Form -->
            
                    <!--begin: Datatable -->
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                            <tr>
                                <th title="Field #1" data-field="service">{$lang.service}</th>
                                <th title="Field #2" data-field="Status">{$lang.status}</th>
                            {if $action=='vps'}
                                <th title="Field #3" data-field="hostname">{$lang.hostname}</th>
                                <th title="Field #4" data-field="ipadd">{$lang.ipadd}</th>
                            {else}
                                <th title="Field #3" data-field="price">{$lang.price}</th>
                                <th title="Field #4" data-field="bcycle">{$lang.bcycle}</th>
                            {/if}
                                <th title="Field #5" data-field="nextdue">{$lang.nextdue}</th>
                                <th title="Field #6" data-field="setting"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {include file='ajax/ajax.services.tpl'}
                        </tbody>
                    </table>
        </div>
    </div>
</div>
        {/if}
    {else}
        <div class="spacing">
            <h3>{$lang[$action]|capitalize}</h3>
            <p>{$lang.nothing}</p>
            {if $cid}
                {foreach from=$offer item=oo}
                    {if $cid==$oo.id && $oo.visible=='1'}
                        <form method="post" action="{$ca_url}cart&cat_id={$cid}" style="text-align: center">
                            <button  class="btn btn-danger"  style="font-weight:bold;font-size:16px;padding:15px 10px;">
                                <i class="icon-shopping-cart icon-white"></i> {$lang.Add} {$oo.name}
                            </button>
                            {securitytoken}
                        </form>
                    {/if}
                {/foreach}
            {/if}
        </div>
    {/if}
{/if}
{literal}
<script>
//== Class definition

var DatatableHtmlTableDemo = function() {
	//== Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.m-datatable').mDatatable({
			data: {
				saveState: {cookie: false},
			},

			columns: [
				{
					field: 'Status',
					title: 'Status',
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Pending', 'class': 'm-badge--brand'},
							2: {'title': 'Delivered', 'class': ' m-badge--metal'},
							3: {'title': 'Canceled', 'class': ' m-badge--primary'},
							4: {'title': 'Success', 'class': ' m-badge--success'},
							5: {'title': 'Info', 'class': ' m-badge--info'},
							6: {'title': 'Danger', 'class': ' m-badge--danger'},
							7: {'title': 'Warning', 'class': ' m-badge--warning'},
						};
						return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
					},
				},
			],
		});

		$('#m_form_status').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Status');
		});
		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		//== Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	DatatableHtmlTableDemo.init();
});
</script>
{/literal}
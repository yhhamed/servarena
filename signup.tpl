<div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                       {$lang.createaccount}
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Oh snap! Change a few things up and try submitting again.
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
								</div>

    <form method="post" action="" id="signupform" name="signupform">
        {include file='ajax.signup.tpl'}

        <div class="text-center">    
                <input type="submit" value="{$lang.submit}" class="btn btn-primary"/>
        </div>

        {securitytoken}
    </form>
                            </div>
                        </div>
                    </div>
 </div>
  </div>
		{literal}<script>
		//== Class definition
var FormControls = function () {
    //== Private functions
    
    var demo1 = function () {
        $( "#signupform" ).validate({
            // define validation rules
            rules: {
                {/literal}{foreach from=$fields item=field name=floop key=k}
                {$k}{literal}: {
                   {/literal}{if $field.options & 2}{literal}required: true,{/literal}{/if}{literal}
                },{/literal}
                {/foreach}{literal}
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });       
    }


    return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

jQuery(document).ready(function() {    
    FormControls.init();
});
</script>
{/literal}
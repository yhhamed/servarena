
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                          {$lang.tickets|capitalize}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-12 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>{$lang.status}:</label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="m_form_status">
                                            <option value="">{$lang.all}</option>
                                            <option value="1">{$lang.Open}</option>
                                            <option value="2">{$lang.Answered}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--end: Search Form -->            <!--begin: Datatable -->
            <table class="m-datatable" id="html_table" width="100%">
                <thead>
                    <tr>
                        <th title="Field #1" data-field="subject">{$lang.subject}</th>
                        <th title="Field #9" data-field="Status">{$lang.status}</th>
                        <th title="Field #2" data-field="expirydate">{$lang.department}</th>
                        <th title="Field #3" data-field="date">{$lang.date}</th>
                        <th title="Field #4" data-field="free"></th>
                    </tr>
                </thead>
                <tbody>
                        {include file='ajax/ajax.tickets.tpl'}
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
{literal}
<script>
//== Class definition
var DatatableHtmlTableDemo = function() {
	//== Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.m-datatable').mDatatable({
			data: {
				saveState: {cookie: false},
			},
			
			columns: [
				{
					field: 'Status',
					title: 'Status',
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': '{/literal}{$lang.Paid}{literal}', 'class': 'm-badge--success'},
							2: {'title': '{/literal}{$lang.Unpaid}{literal}', 'class': ' m-badge--danger'},
						};
						return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
					},
				},
			],
		});

		$('#m_form_status').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Status');
		});

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		//== Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	DatatableHtmlTableDemo.init();
});
</script>
{/literal}